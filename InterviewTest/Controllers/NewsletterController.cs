﻿using System.Collections;
using System.Linq;
using System.Web.Mvc;
using InterviewTest.Database;
using InterviewTest.Extensions;
using InterviewTest.Models;
using InterviewTest.Models.ViewModels;
using Microsoft.Ajax.Utilities;

namespace InterviewTest.Controllers
{
    public class NewsletterController : Controller
    {
        private static FileSystemDatabase GetDatabase() => new FileSystemDatabase();

        public ActionResult Create(int count)
        {
            var db = GetDatabase();

            var hostIds = db.GetAll<Host>().Select(h => h.Id).ToArray();
            var tripIds = db.GetAll<Trip>().Select(t => t.Id).ToArray();
            var newsLetterConfig = db.GetAll<NewsletterConfig>().FirstOrDefault() ?? new NewsletterConfig();

            var HostIds = new Stack(Enumerable.Range(0, 2).Select(x => hostIds.GetRandom()).ToList());
            var TripIds = new Stack(Enumerable.Range(0, 2).Select(x => tripIds.GetRandom()).ToList());
            for (var i = 0; i < count; i++)
            {
                var newsletter = new Newsletter();
                newsletter.NewsletterConfig = newsLetterConfig.NewsletterFormat;

                foreach (char item in newsletter.NewsletterConfig)
                {
                    if (item.Equals('T') || item.Equals('t'))
                    {
                        if (TripIds.Count > 0)
                            newsletter.TripIds.Push(TripIds.Pop().ToString());
                    }
                    else if (item.Equals('H') || item.Equals('h'))
                    {
                        if (HostIds.Count > 0)
                            newsletter.HostIds.Push(HostIds.Pop().ToString());
                    }
                }
                db.Save(newsletter);
            }

            TempData["notification"] = "Created {count} newsletters";
            return RedirectToAction("list");
        }

        public ActionResult DeleteAll()
        {
            GetDatabase().DeleteAll<Newsletter>();

            TempData["notification"] = "All newsletters deleted";

            return RedirectToAction("list");
        }

        public ActionResult Display(string id)
        {
            var db = GetDatabase();

            var newsletter = db.Get<Newsletter>(id);

            var viewModel = new NewsletterViewModel();
            foreach (char item in newsletter.NewsletterConfig)
                if (item.Equals('T') || item.Equals('t'))
                {
                    if (newsletter.TripIds.Count > 0)
                        viewModel.Items.Push(NewsletterTripViewModel.Convert(db.Get<Trip>(newsletter.TripIds.Pop())));
                }
                else if (item.Equals('H') || item.Equals('h'))
                {
                    if(newsletter.HostIds.Count > 0)
                    viewModel.Items.Push(NewsletterHostViewModel.Convert(db.Get<Host>(newsletter.HostIds.Pop())));
                }

            return View("Newsletter", viewModel);
        }

        public ActionResult Sample()
        {
            var viewModel = new NewsletterViewModel()
            {
                Items =
                {
                    //NewsletterHostViewModel.Convert(EntityGenerator.GenerateHost()),
                    //NewsletterTripViewModel.Convert(EntityGenerator.GenerateTrip(null), "Test host name"),
                    //NewsletterTripViewModel.Convert(EntityGenerator.GenerateTrip(null), "Test host name"),
                    //NewsletterHostViewModel.Convert(EntityGenerator.GenerateHost()),
                }
            };

            return View("Newsletter", viewModel);
        }

        public ActionResult List()
        {
            var viewModel = new NewsletterListViewModel
            {
                Newsletters = GetDatabase().GetAll<Newsletter>(),
            };

            return View(viewModel);
        }

        public ActionResult Config()
        {
            var firstOrDefault = GetDatabase().GetAll<NewsletterConfig>().FirstOrDefault();
            NewsletterConfigViewModel viewModel;
            if (firstOrDefault != null)
            {
                viewModel = new NewsletterConfigViewModel
                {
                    NewsletterFormat = firstOrDefault.NewsletterFormat
                };
            }
            else
            {
                viewModel = new NewsletterConfigViewModel
                {
                    NewsletterFormat = new NewsletterConfig().NewsletterFormat
                };
            }
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Config(FormCollection collection)
        {
            var db = GetDatabase();
            var newsletterFormat = collection.Get("NewsletterFormatInput");
            if (newsletterFormat.IsNullOrWhiteSpace()) return Redirect("Config");
            if (db.GetAll<NewsletterConfig>().FirstOrDefault() != null)
                db.DeleteAll<NewsletterConfig>();

            NewsletterConfig newsletterConfig = new NewsletterConfig { NewsletterFormat = newsletterFormat };
            db.Save(newsletterConfig);
            return Redirect("Config");
        }
    }
}