﻿using System;
using System.Collections.Generic;
using InterviewTest.Database;

namespace InterviewTest.Models
{
    public class Newsletter : IPersistable
    {
        public Newsletter()
        {
            CreatedAt = DateTime.Now;
            TripIds = new Stack<string>();
            HostIds = new Stack<string>();
        }

        public string Id { get; set; }
        public Stack<string> TripIds { get; set; }
        public Stack<string> HostIds { get; set; }
        public string NewsletterConfig { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}