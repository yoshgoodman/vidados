﻿namespace InterviewTest.Models.ViewModels
{
    public class NewsletterConfigViewModel
    {
        public string NewsletterFormat { get; set; }
    }
}