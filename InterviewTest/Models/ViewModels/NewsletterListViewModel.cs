﻿using System.Collections.Generic;

namespace InterviewTest.Models.ViewModels
{
    public class NewsletterListViewModel
    {
        public List<Newsletter> Newsletters { get; set; }
    }
}