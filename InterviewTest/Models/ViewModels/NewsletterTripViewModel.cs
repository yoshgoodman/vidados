﻿using InterviewTest.Database;

namespace InterviewTest.Models.ViewModels
{
    public class NewsletterTripViewModel
    {
        private static FileSystemDatabase GetDatabase() => new FileSystemDatabase();

        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Country { get; set; }
        public string HostName { get; set; }
        public string ImageUrl { get; set; }

        public static NewsletterTripViewModel Convert(Trip trip, string hostName = null)
        {
            return new NewsletterTripViewModel
            {
                Name = trip.Name,
                Country = trip.Country,
                HostName = hostName ?? GetDatabase().Get<Host>(trip.HostId)?.Name,
                ImageUrl = trip.ImageUrl
            };
        }
    }
}