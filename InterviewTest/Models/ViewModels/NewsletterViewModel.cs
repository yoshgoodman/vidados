﻿using System.Collections.Generic;

namespace InterviewTest.Models.ViewModels
{
    public class NewsletterViewModel
    {
        public NewsletterViewModel()
        {
            Items = new Stack<object>();
        }

        public Stack<object> Items { get; set; } 
    }
}