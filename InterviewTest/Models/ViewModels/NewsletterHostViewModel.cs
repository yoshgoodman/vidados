﻿
namespace InterviewTest.Models.ViewModels
{
    public class NewsletterHostViewModel
    {
        public string Name { get; set; }
        public string Job { get; set; }
        public string ImageUrl { get; set; }

        public static NewsletterHostViewModel Convert(Host host)
        {
            return new NewsletterHostViewModel
            {
                Name = host.Name,
                ImageUrl = host.ImageUrl,
                Job = host.Job
            };
        }
    }
}