﻿using System;
using InterviewTest.Database;

namespace InterviewTest.Models
{
    public class NewsletterConfig : IPersistable
    {
        public NewsletterConfig()
        {
            CreatedAt = DateTime.Now;
            NewsletterFormat = "TTTHHTTT";
        }
        public string Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string NewsletterFormat { get; set; }
    }
}