﻿using InterviewTest.Database;

namespace InterviewTest.Models
{
    public class TravelItem : IPersistable
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }

    public class Trip : TravelItem
    {
        public string Country { get; set; }
        public string HostId { get; set; }
    }

    public class Host : TravelItem
    {
        public string Job { get; set; }
    }
}